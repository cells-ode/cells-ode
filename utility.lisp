
#|

    Cells-ODE -- A cells driven interface to cl-ode

Copyright (C) 2008 by Peter Hildebrandt

This library is free software; you can redistribute it and/or
modify it under the terms of the Lisp Lesser GNU Public License
 (http://opensource.franz.com/preamble.html), known as the LLGPL.

This library is distributed  WITHOUT ANY WARRANTY; without even 
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  

See the Lisp Lesser GNU Public License for more details.

|#


;;;
;;; utilty funcs --------------------------------------------------------------------------
;;;

(in-package :c-ode)

;;; this is also defined in bodies.lisp, but so important that we re-wrap it here
(def-ode-method (relative->world-coords :ode-name get-rel-point-pos) ((self body) (point vector) (result vector)))


;;; TODO rotation functions
;;; these rely heavily on the matrix type which is not supported by cl-ode




