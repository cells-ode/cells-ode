#|

    Cells-ODE -- A cells driven interface to cl-ode

Copyright (C) 2008 by Peter Hildebrandt

This library is free software; you can redistribute it and/or
modify it under the terms of the Lisp Lesser GNU Public License
 (http://opensource.franz.com/preamble.html), known as the LLGPL.

This library is distributed  WITHOUT ANY WARRANTY; without even 
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  

See the Lisp Lesser GNU Public License for more details.

|#


;;;
;;; ODE Type conversion
;;;

(in-package :c-ode)

(defconstant +precision+ 'single-float)
(define-constant +infinity+ 1.0e8 "prevent overflows")

(eval-now!  
;;; unknown type
  (defmethod make-with-ode (name type body &optional (self 'self))
    (declare (ignorable self name))
    (error "unknown ode call type ~a:~% ~a" type body))
  (defmethod make-from-ode (type result-arg body)
    (declare (ignorable  result-arg))
    (error "unknown ode return type ~a:~% ~a" type body))
  (defmethod make-convert (name type)
    (error "unknown ode variable type ~a: ~a" type name))
  

;;; none
  
  (defmethod make-with-ode (name (type (eql nil)) body &optional (self 'self))
    (declare (ignorable self name))
    `(progn ,@body))

  (defmethod make-convert (name (type (eql nil))) `(,name))

  (defmethod make-from-ode ((type (eql nil)) result-arg body)
    (declare (ignorable  result-arg))
    `(progn ,@body))

;;; number
  
  (defmethod make-with-ode (name (type (eql 'number)) body &optional (self 'self))
    (declare (ignorable self name))
    `(progn ,@body))

  (defmethod make-convert (name (type (eql 'number))) `((coerce ,name +precision+)))
  
  (defmethod make-from-ode ((type (eql 'number)) result-arg body)
    (when result-arg (error "cannot return number through parameter ~a: ~a" result-arg body))
    `(progn ,@body))

  
;;; int
  
  (defmethod make-with-ode (name (type (eql 'int)) body &optional (self 'self))
    (declare (ignorable self name))
    `(progn ,@body))

  (defmethod make-convert (name (type (eql 'int))) `((coerce ,name 'integer)))
  
  (defmethod make-from-ode ((type (eql 'int)) result-arg body)
    (when result-arg (error "cannot return number through parameter ~a: ~a" result-arg body))
    `(progn ,@body))

;;; object

  (defmethod make-with-ode (name (type (eql 'object)) body &optional (self 'self))
    (declare (ignorable self name))
    `(progn ,@body))

  (defmethod make-convert (name (type (eql 'object))) `((id ,name)))
  
  (defmethod make-from-ode ((type (eql 'object)) result-arg body)
    (when result-arg (error "cannot return object through parameter ~a: ~a" result-arg body))
    `(lookup (progn ,@body)))

;;; mass

  
  (defmethod make-with-ode (name (type (eql 'mass)) body &optional (self 'self))
    (declare (ignorable self name))
    `(progn ,@body))

  (defmethod make-convert (name (type (eql 'mass))) `((mass-obj ,name)))
  
  (defmethod make-from-ode ((type (eql 'mass)) result-arg body)
    `(lookup ,(if result-arg
		  `(let ((,result-arg (make-instance 'mass))) ,@body)
		  `(progn ,@body))))
  
;;; vector

  (defmethod make-with-ode (name (type (eql 'vector)) body &optional (self 'self))
    (declare (ignorable self name))
    `(progn ,@body))

  (defmethod make-convert (name (type (eql 'vector)))
    (loop for i from 0 below 3 collecting `(coerce (aref ,name ,i) +precision+)))

  (defmethod make-from-ode ((type (eql 'vector)) result-arg body)
    (let ((rest `(coerce (loop for i from 0 below 3
			    collect (mem-aref ,(or result-arg 'ptr) 'ode:real i))
			 'vector)))
      (if result-arg
	  `(with-foreign-object (,result-arg 'ode:real 3)
	     ,@body
	     ,rest)
	  `(let ((ptr (progn ,@body)))
	     ,rest))))

;;; vector-4  

  
  (defmethod make-with-ode (name (type (eql 'vector-4)) body &optional (self 'self))
    (declare (ignorable self name))
    `(progn ,@body))

  (defmethod make-convert (name (type (eql 'vector-4)))
    (loop for i from 0 below 4 collecting `(coerce (aref ,name ,i) +precision+)))

  (defmethod make-from-ode ((type (eql 'vector-4)) result-arg body)
    (let ((rest `(coerce (loop for i from 0 below 4
			    collect (mem-aref ,(or result-arg 'ptr) 'ode:real i))
			 'vector)))
      (if result-arg
	  `(with-foreign-object (,result-arg 'real 4)
	     ,@body
	     ,rest)
	  `(let ((ptr (progn ,@body)))
	     ,rest))))

  ;;; vector-3-ptr

  (defmethod make-with-ode (name (type (eql 'vector-3-ptr)) body &optional (self 'self))
    (declare (ignorable self name))
    (let ((vec (intern-string name type)))
      `(with-foreign-object (,vec 'ode:real 3)
	 ,@(loop for i from 0 below 3
		collect `(setf (mem-aref ,vec 'ode:real ,i) (coerce (aref ,name ,i) +precision+)))
	 ,@body)))

  (defmethod make-convert (name (type (eql 'vector-3-ptr)))
    `(,(intern-string name type)))
  
;;; quaternion
    
  
  (defmethod make-with-ode (name (type (eql 'quaternion)) body &optional (self 'self))
    (declare (ignorable self name))
    (let ((quat (intern-string name type)))
      `(with-foreign-object (,quat 'ode:quaternion 4)
	 (let ((axis-angle (vector (aref ,name 1) (aref ,name 2) (aref ,name 3) (aref ,name 0))))
	   (call-ode qfrom-axis-and-angle (,quat (axis-angle vector-4)))
	   ,@body))))

  (defmethod make-convert (name (type (eql 'quaternion)))
    `(,(intern-string name type)))
  
  (defmethod make-from-ode ((type (eql 'quaternion)) result-arg body)    
    `(let* ((q ,(make-from-ode 'vector-4 result-arg body))
	    ;; parse quaternion (p49 ODE manual)
	    (theta (* 2.0 (acos (aref q 0))))
	    (s (sin (/ theta 2d0))))
       (if (not (= 0 s))
	   (vector theta
		   (/ (aref q 1) s)
		   (/ (aref q 2) s)
		   (/ (aref q 3) s))
	   (vector theta 0 0 0))))

;;; boolean
  

  (defmethod make-with-ode (name (type (eql 'bool)) body &optional (self 'self))
    (declare (ignorable self name))
    `(progn ,@body))

  (defmethod make-convert (name (type (eql 'bool))) `((if ,name 1 0)))
  
  (defmethod make-from-ode ((type (eql 'bool)) result-arg body)
    (when result-arg (error "cannot return boolean value through variable ~a: ~a" result-arg body))
    `(not (eql (progn ,@body) 0)))
  
;;; generic macros
  
  
  (defmacro with-ode-val ((name type &optional (self 'self)) &body body)
    (make-with-ode name type body self))

  (defmacro convert (name type)
    (make-convert name type))

  (defmacro with-ode-return (type (&optional result-arg) &body body)
    (make-from-ode type result-arg body)))

