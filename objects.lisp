#|

    Cells-ODE -- A cells driven interface to cl-ode

Copyright (C) 2008 by Peter Hildebrandt

This library is free software; you can redistribute it and/or
modify it under the terms of the Lisp Lesser GNU Public License
 (http://opensource.franz.com/preamble.html), known as the LLGPL.

This library is distributed  WITHOUT ANY WARRANTY; without even 
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  

See the Lisp Lesser GNU Public License for more details.

|#



;;;
;;; objects
;;;


(in-package :c-ode)


;;;
;;; superclass for ode objects
;;;

(defmodel ode-object (family)
  ((ode-id :initarg :ode-id :cell nil :accessor ode-id)))


;;; cells-ode bookkeeping

(defvar *objects* nil "hashtable of all objects created")
(defvar *objects-by-name* (make-hash-table :test 'equalp))

(defmethod id ((self ode-object))
  (ode-id self))

(defmethod reg-ode ((self ode-object))
  (when *objects*
    (if (id self)
	(progn
	  (when (gethash (pointer-address (id self)) *objects*)
	    (warn "OVERWRITING OBJECT ~a with key ~a" self (id self)))
	  (setf (gethash (pointer-address (id self)) *objects*) self))
	(push self (gethash nil *objects*))))
  (setf (gethash (md-name self) *objects-by-name*) self))

(defmethod unreg-ode ((self ode-object))
  (when *objects*
   (if (id self)
       (remhash (pointer-address (id self)) *objects*)
       (setf (gethash nil *objects*)
	     (delete self (gethash nil *objects*)))))
  #+msg(format t "~&unreg ~a (is curr ~a)" (md-name self) (gethash (md-name self) *objects-by-name*))
  (remhash (md-name self) *objects-by-name*)
  #+msg(format t " --> is now ~a~%" (gethash (md-name self) *objects-by-name*)))

(defun lookup (id)
  (when *objects*
   (if id
       (gethash (pointer-address id) *objects*)
       (cerror "Ignore" "Tried to lookup ID nil.  Nil objects are ~a" (gethash nil *objects*)))))

(defun obj (name)
  (gethash name *objects-by-name*))



;;;
;;; base methods on ode object
;;;

(defmethod initialize-instance :after ((self ode-object) &key &allow-other-keys)
  "register object"
  (reg-ode self))

(defmethod update ((self ode-object))
  "called to update cells model after step"
  self)

(defmethod ode-destroy ((self ode-object))
  "finalize object"
  (unreg-ode self))

(defmethod md-awaken :after ((self ode-object))
  (update self))


;;;
;;; simple class ordering
;;;

(define-constant +ode-classes+ '(joint general-geom body mass space world))

(defun major-class-name (obj)
  (dolist (class +ode-classes+)
    (when (typep obj class)
      (return class))))


;;;
;;; printing
;;; 


(defgeneric echo-slots (self)
  (:method-combination append))

(defmethod echo-slots append ((self ode-object))
  '())

(defmethod print-object ((self ode-object) stream)
  (format stream "#<~a ~a {~{~#[~;~{~#[~;~a~:;~a: ~]~}~:;~{~#[~;~a; ~:;~a: ~]~}~]~}}>"
	  (type-of self) (md-name self)
	  (mapcar #'(lambda (slot) (list slot (funcall (symbol-function slot) self)))
		  (echo-slots self)))
  self)

;;;
;;; collidable object
;;;

(def-ode-model collideable-object ()
  ((mu :ode nil :cell nil :initform +infinity+)
   (soft-erp :ode nil :cell nil :initform .2)
   (soft-cfm :ode nil :cell nil :initform .00001)
   (bounce :ode nil :cell nil :initform 0.5)
   (bounce-vel :ode nil :cell nil :initform 2.0)
   (slip-1 :ode nil :cell nil :initform 0.1)
   (slip-2 :ode nil :cell nil :initform 0.1)))