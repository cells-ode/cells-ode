#|

    Cells-ODE -- A cells driven interface to cl-ode

Copyright (C) 2008 by Peter Hildebrandt

This library is free software; you can redistribute it and/or
modify it under the terms of the Lisp Lesser GNU Public License
 (http://opensource.franz.com/preamble.html), known as the LLGPL.

This library is distributed  WITHOUT ANY WARRANTY; without even 
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  

See the Lisp Lesser GNU Public License for more details.

|#


(in-package :cells-ode)


;;;
;;; General purpose utilities
;;;

;;; kt> in utils-kt
;;;(eval-when (:compile-toplevel :load-toplevel :execute)
;;;  (defmacro eval-now! (&body body)
;;;    `(eval-when (:compile-toplevel :load-toplevel :execute)
;;;       ,@body)))

(eval-now!
  (defun mk-list (var)
    (if (listp var) var (list var)))
  
  (defmacro nconcf (place add)
    `(setf ,place (nconc (mk-list ,place) (mk-list ,add))))

  (defmacro with-uniqs (syms &body body)
    `(let ,(mapcar #'(lambda (sym) `(,sym (gensym ,(concatenate 'string (string sym) "-")))) (mk-list syms))
       ,@body))
  
  (defmacro csetf (place value)
    (with-uniqs newval
      `(let ((,newval ,value))
	 (unless (eql ,newval ,place)
	   (setf ,place ,newval)))))

  (defmacro dohash ((obj hash-table) &body body)
    `(loop for ,obj being the hash-values of ,hash-table do ,@body))
  
  (defun denil (lst)
    (loop for x in lst if x collect x))
  (defun concat (&rest parts)
    (format nil "~:@(~{~@[~a~#[~:;-~]~]~}~)" (denil parts)))
 
  (defun intern-string (&rest strings)
    (intern (apply #'concat strings))))

;;; ODE function names

(eval-now!
 (defun setter (name slot)
   (intern-string name 'set slot))
 (defun getter (name slot)
   (intern-string name 'get slot)))

;;; deactivating an observer

;; later

;;;
;;; ODE model, method, function, call
;;; 

(defvar *dbg* nil)

(defmacro with-dbg (&body body)
  `(let ((*dbg* t))
     ,@body))

(eval-now!
 (defun make-call (fn ret-type args-and-types &optional (self 'self))
   (multiple-value-bind (args types) (parse-typed-args args-and-types)
     (declare (ignorable types))
     (let (par-list result-arg-type)
       (labels ((call-with (args types)
                  (let ((arg (car args))
                        (type (car types)))
                    (cond
                     ((not args)
                      (let ((fn-call `(,(intern (string fn) :ode) ,@par-list)))
                        (if result-arg-type
                            `(progn ,fn-call result)
                          fn-call)))
                     ((eq arg 'result)
                      (setf result-arg-type type)
                      (nconcf par-list arg)
                      (call-with (rest args) (rest types)))
                     (t
                      (nconcf par-list (make-convert arg type))
                      (make-with-ode arg type (list (call-with (rest args) (rest types))) self))))))	  
         (let ((fn-call (call-with args types)))	    
           ;kt> (trc "fn-call" fn-call)
           (let ((fn-call-ret (bif (return-type (or ret-type result-arg-type))
                                (make-from-ode return-type (when result-arg-type 'result) (list fn-call))
                                fn-call)))
             (with-uniqs result
               `(if *dbg*
                    (progn
                      (format t ,(format nil "~&~%Calling ~a (~~@{~~a~~#[~~:; ~~]~~}) ... " fn) ,@(remove 'result args))
                      (let ((,result ,fn-call-ret))
                        (format t "==> ~a~%" ,result)
                        ,result))
                  ,fn-call-ret))))))))
 
 (defun canonic-args-list (args-and-types)
   (mapcar #'mk-list args-and-types))
 
 (defun parse-typed-args (args-and-types)
   (loop for (arg type) in (canonic-args-list args-and-types)
       collect arg into args
       collect type into types
       finally (return (values args types))))
 
 (defmacro call-ode (fn (&rest args-and-types) &optional return-type (self 'self))
   (make-call fn return-type (canonic-args-list args-and-types) self))
 
 (defmacro def-ode-fun (name (&rest args-and-types) &body ret-type-and-body)
   (destructuring-bind (return-type &rest body) (or ret-type-and-body '(nil))
     (multiple-value-bind (args types)
         (parse-typed-args args-and-types)
       (declare (ignorable types))
       `(progn
          (defun ,name (,@(remove 'result args))
            (macrolet ((call-ode-fun ()
                         '(call-ode ,name (,@args-and-types) ,return-type)))
              ,@(or body '((call-ode-fun)))))
          (eval-now!
           (export '(,name)))))))
 
 
 (defmacro def-ode-method (name ((self class &optional (ode-class class)) &rest args-and-types) &body ret-type-and-body)
   (destructuring-bind (name &key (ode-name name)) (mk-list name)
     (destructuring-bind (return-type &rest body) (or ret-type-and-body '(nil))
       (multiple-value-bind (args types)
           (parse-typed-args args-and-types)
         (declare (ignorable types))
         `(progn
            (defmethod ,name ((,self ,class) ,@(remove 'result args))
              (macrolet ((call-ode-method ()
                           '(call-ode ,(intern-string ode-class ode-name) ((,self object) ,@args-and-types) ,return-type ,self)))
                ,@(or body '((call-ode-method)))))
            (eval-now! (export '(,name)))))))))

;;;
;;; def-ode-model facility
;;; 

(eval-now!
  (defmacro def-ode-model (name (&rest superclasses) slots &rest inits)
    (destructuring-bind (name &key (ode-class name) (joint-axes 0) (ode-joint name)) (mk-list name)
      (loop for raw-slot in (append slots (loop for axis from 1 upto joint-axes
					     nconcing  (mapcar #'(lambda (param)
								   (destructuring-bind (name &optional (init 0)) param
								     `(,(if (= axis 1) name (intern-string name axis)) :initform (c-in ,init) :joint-param t)))
							       '((lo-stop (- +infinity+))
								 (hi-stop +infinity+)
								 (vel)
								 (f-max)
								 (fudge-factor 1)
								 (bounce)
								 (cfm)
								 (stop-erp)
								 (stop-cfm)
								 (suspension-erp)
								 (suspension-cfm)))
					     ))
	 for slot = (if (listp raw-slot) raw-slot (list raw-slot))
	 for (slotdef observer updater export)
	 = (destructuring-bind (slotname &key
					 (ode t)
					 (read-only nil)
					 (joint-param nil)
					 (type 'number)
					 (auto-update (or read-only
							  (and (not (member type '(mass object)))
							       (not joint-param))))
					 (ode-slot (cond (joint-param (concat ode-joint 'param))
							 ((eq ode-class 'joint) (concat ode-joint slotname))
							 (t slotname)))
					 (initform (if read-only
						       (case type
							 ((or number int) '(c-in 0))
							 (vector '(c-in #(0 0 0)))
							 (quaternion '(c-in #(0 0 1 0)))
							 (t '(c-in nil)))
						       '(c-in nil)))
					 (result-arg (member type '(mass)))
					 (cell t)) slot
	     (let ((slot$ (intern (string slotname))))
	       `((,slotname :initarg ,(intern (string slotname) :keyword)
			    :initform ,initform
			    :accessor ,slot$
			    :cell ,cell)
		 ,(when (and ode (not read-only))
			`(defobserver ,slot$ ((self ,name) newval)
			   ,(let ((ode-call `(call-ode ,(setter ode-class ode-slot)
						       ((self object)
							,@(append (when joint-param
								    (list (intern (format nil "+PARAM-~a+" slotname) :ode)))
								  `((newval ,type)))) nil self)))
				 (if (eq type 'bool)
				     ode-call
				     `(when newval ,ode-call)))))
		 ,(when (and ode auto-update)
			`(csetf (,slot$ self) (call-ode ,(getter ode-class ode-slot)
							,@(if result-arg
							      `(((self object) (result ,type)))
							      `(((self object)) ,type)))))
		 ,slot$)))
	 if slotdef collect slotdef into slotdefs
	 if observer collect observer into observers
	 if updater collect updater into updaters
	 if export collect export into exports
	 finally (return `(progn
			    (eval-now!
			      (defmodel ,name (,@(if (member 'ode-object superclasses) superclasses (append superclasses '(ode-object))))
				,slotdefs
				,@inits)
			      ,@observers)
			    (defmethod update ((self ,name))
			      ,@updaters
			      (call-next-method))
			    (eval-now! (export ',(append (list name) exports)))))))))



