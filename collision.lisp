#|

    Cells-ODE -- A cells driven interface to cl-ode

Copyright (C) 2008 by Peter Hildebrandt

This library is free software; you can redistribute it and/or
modify it under the terms of the Lisp Lesser GNU Public License
 (http://opensource.franz.com/preamble.html), known as the LLGPL.

This library is distributed  WITHOUT ANY WARRANTY; without even 
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  

See the Lisp Lesser GNU Public License for more details.

|#


;;; -----------------------------------------------------------------------------------------------
;;;     collision detection
;;; -----------------------------------------------------------------------------------------------

(in-package :c-ode)

;;;
;;; Spaces
;;; 

(def-ode-model space ()
  ((cleanup :type bool :initform (c-in t)) ; automatic cleanup
   (num-geoms :type int :read-only t))
  )

(defmethod ode-destroy ((self space))
  (call-ode space-destroy ((self object)))
  (call-next-method))

(defmethod echo-slots append ((self space))
  '(num-geoms))

;;; simple space

(def-ode-model simple-space (space)
  ()
  (:default-initargs
      :ode-id (call-ode simple-space-create (((null-pointer))))))


;;; hash space

(def-ode-model hash-space (space)
  ()
  (:default-initargs
      :ode-id (call-ode hash-space-create (((null-pointer))))))

(def-ode-method set-levels ((self hash-space) (minlevel int) (maxlevel int)))

;;; TODO (def-ode-method get-levels) ;; needs multiple return values

;;; quad tree space

(def-ode-model quad-tree-space (space)
  ()
  (:default-initargs
      :ode-id (error "Use mk-quad-tree-space to create a quad-tree-space")))

(defun mk-quad-tree-space (center extents depth &rest initargs)
  (apply #'make-instance
	 'quad-tree-space
	 :ode-id (call-ode quad-tree-space-create (((null-pointer)) (center vector-3-ptr) (extents vector-3-ptr) (depth int)))
	 initargs))


;;;
;;; geom/space bookkeeping
;;;

(def-ode-method (add-geom :ode-name add) ((self space) (geom object)))
(def-ode-method (remove-geom :ode-name remove) ((self space) (geom object)))

(def-ode-method (query-geom :ode-name query) ((self space) (geom object)) bool)

(def-ode-method get-geom ((self space) (num int)) object)

(defmethod geoms ((self space))
  (bwhen (num (num-geoms self))
   (loop for i from 0 below num collecting (get-geom self i))))

;;;
;;; collision detection
;;; 

(eval-now! ;; kt> needed at compile-time for do-contacts defmacro
 (defconstant +max-collision-contacts+ 256))

(defvar *collision-joint-group* nil "ODE joint group")


(def-ode-method (space-collide :ode-name collide) ((self space) data near-collision-callback))
(def-ode-fun space-collide2 ((geom-1 object) (geom-2 object) data near-collision-callback))
(def-ode-fun collide ((geom-1 object) (geom-2 object) (max-contacts int) contact (skip int)) int
  (format t "~&in collide~%")
  (let ((res (call-ode-fun)))
    (format t "~&called collide -- result ~a~%" res)
    res))

(defmacro do-contacts ((contact geom-1 geom-2 &key (max-contacts +max-collision-contacts+)) &body body)
  (with-uniqs (contacts num-contacts)
    `(with-foreign-object (,contacts 'ode:contact ,max-contacts)
       (let ((,num-contacts (call-ode collide ((,geom-1 object) 
					       (,geom-2 object)
					       (,max-contacts int)
					       ((foreign-slot-value (mem-aref ,contacts 'ode:contact 0) 'ode:contact 'ode:geom))
					       ((foreign-type-size 'ode:contact))) int)

	       ))
	 (dotimes (i ,num-contacts)
	   (let ((,contact (mem-aref ,contacts 'ode:contact i)))
	     (flet ((mk-collision () (attach (mk-contact-joint *collision-joint-group* ,contact) (body ,geom-1) (body ,geom-2))))
	      ,@body)))))))

(eval-now!
 (defun ode-sym (sym)
   (intern (string sym) :ode))
 
 (defun make-with (type slots-and-types)
   (multiple-value-bind (slots types) (parse-typed-args slots-and-types)
     `(defmacro ,(intern-string 'with type) (,type (&optional ,@(mapcar #'(lambda (slot) `(,slot ',(gensym (string slot)))) slots)) &body body)
	(declare (ignorable ,@slots))
	(list 'with-foreign-slots (list ',(mapcar #'ode-sym slots) ,type ',(ode-sym type))
	      (append (list 'let (list ,@(mapcar #'(lambda (slot type) `(list ,slot ',(make-from-ode type nil (list (ode-sym slot))))) slots types)))
		      (list (list 'declare (append '(ignorable) ,(append '(list) slots))))
		      body))))))

(defmacro def-with-ode (type (&rest slots-and-types))
  (make-with type slots-and-types))

(def-with-ode contact (surface geom (f-dir-1 vector)))

(def-with-ode contact-geom ((pos vector) (normal vector) (g-1 object) (g-2 object) (depth number) (side-1 int) (side-2 int)))


(defmacro with-surface-parameters ((ode-surface geom-1 geom-2) select &body body)
  (let ((params '(mu slip-1 slip-2 soft-erp bounce bounce-vel soft-cfm)))
    (let ((ode-params (mapcar #'(lambda (sym) (intern (string sym) :ode)) params)))
      (with-uniqs mode
	`(with-foreign-slots (,(append ode-params '(ode:mode)) ,ode-surface ode:surface-parameters)
	   (let ,(append (list (list mode 0)) params)
	     (macrolet ((select-max (&rest params) `(progn ,@(mapcar #'(lambda (param) `(setf ,param (max (,param ,',geom-1) (,param ,',geom-2)))) params)))
			(select-avg (&rest params) `(progn ,@(mapcar #'(lambda (param) `(setf ,param (/ (+ (,param ,',geom-1) (,param ,',geom-2)) 2))) params)))
			(select-min (&rest params) `(progn ,@(mapcar #'(lambda (param) `(setf ,param (min (,param ,',geom-1) (,param ,',geom-2)))) params)))
)
	       ,select)
	     ,@(loop for sym in params for ode-sym in ode-params
		  collecting `(when ,sym
				(setf ,ode-sym ,@(make-convert sym 'number))
				(setf ,mode (logior ,mode
						    ,(intern (format nil "+CONTACT-~a+" (case sym
											  (bounce-vel 'bounce)
											  (mu 'approx-1)
											  (t sym)))
							     :ode)))))
	     (setf ,(intern "MODE" :ode) ,mode)
	     ,@body))))))

;;;
;;; collision detection callback
;;; 

(defcallback near-collision-callback :void ((data :pointer) (geom-id-1 ode:geom-id) (geom-id-2 ode:geom-id))
  (let ((geom-1 (lookup geom-id-1))
        (geom-2 (lookup geom-id-2)))
    (if (or (is-space geom-1) (is-space geom-2))
        (space-collide2 geom-1 geom-2 data (callback near-collision-callback)) ;; kt> made it -collide2
      (progn
        (format t "~&Colliding geoms ~a <--> ~a~%" (md-name geom-1) (md-name geom-2))
        (do-contacts (contact geom-1 geom-2)
          (with-contact contact (surface contact-geom friction-dir-1)
            (with-contact-geom contact-geom (pos normal)
              (with-surface-parameters (surface geom-1 geom-2)
                (progn (select-min mu)
		       (select-avg bounce-vel)
		       (select-max bounce))
                (mk-collision)))))))))

;;;
;;; high level collision detection routine

(defmacro with-collision ((space) &body body)
  `(let ((*collision-joint-group* (mk-joint-group (* +max-collision-contacts+ 1000))))
     (space-collide ,space (null-pointer) (callback near-collision-callback))
     ,@body
     (ode-destroy *collision-joint-group*)))
