#|

    Cells-ODE -- A cells driven interface to cl-ode

Copyright (C) 2008 by Peter Hildebrandt

This library is free software; you can redistribute it and/or
modify it under the terms of the Lisp Lesser GNU Public License
 (http://opensource.franz.com/preamble.html), known as the LLGPL.

This library is distributed  WITHOUT ANY WARRANTY; without even 
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  

See the Lisp Lesser GNU Public License for more details.

|#


(in-package :cells-ode)


;;;; -----------------------------------------------------------------------------------------------
;;;;     world and environment
;;;; -----------------------------------------------------------------------------------------------


(def-ode-model environment (collideable-object)
  ()
  (:default-initargs
      :ode-id (null-pointer)
    :md-name :environment))

(defparameter *environment* (make-instance 'environment) "static environment")

;;;
;;; world
;;; 

;;; ODE variables


(defvar *world* nil "ODE world")
(export! *world*)

(def-ode-model world ()
  ((gravity :type vector :initform (c-in #(0 0 -9.81)) :auto-update nil)
   (erp :initform (c-in .4) :auto-update nil)
   (cfm :initform (c-in .00001) :auto-update nil)
   (auto-disable-flag :type bool :auto-update nil)
   (auto-disable-linear-threshold  :auto-update nil)
   (auto-disable-angular-threshold  :auto-update nil)
   (auto-disable-steps :type int :auto-update nil)
   (auto-disable-time :auto-update nil)
   (quick-step-num-iterations :type int :initform (c-in 20) :auto-update nil)
   (contact-max-correcting-vel :auto-update nil)
   (contact-surface-layer :auto-update nil))
  (:default-initargs
      :ode-id (call-ode world-create ())
    :md-name :world))

(defmethod initialize-instance :after ((self world) &rest initargs)
  (declare (ignore initargs))
  (setf *world* self))

(defmethod ode-destroy ((self world))
  (call-ode world-destroy ((self object)))
  (setf *world* nil)
  (call-next-method))

(def-ode-method impulse-to-force ((self world) (step-size number) (impulse vector) (result vector)))

(def-ode-method step ((self world) (step-size number)))
(def-ode-method step-fast1 ((self world) (step-size number) (max-iterations int)))




