#|

    Cells-ODE -- A cells driven interface to cl-ode

Copyright (C) 2008 by Peter Hildebrandt

This library is free software; you can redistribute it and/or
modify it under the terms of the Lisp Lesser GNU Public License
 (http://opensource.franz.com/preamble.html), known as the LLGPL.

This library is distributed  WITHOUT ANY WARRANTY; without even 
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  

See the Lisp Lesser GNU Public License for more details.

|#


(in-package :cl-user)

(defpackage :cells-ode
  (:nicknames :c-ode :co)
  (:use :cl :cells :cffi :utils-kt)
  (:shadow
   #:position #:length #:step #:space)
  (:export
   ))
