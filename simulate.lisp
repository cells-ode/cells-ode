#|

    Cells-ODE -- A cells driven interface to cl-ode

Copyright (C) 2008 by Peter Hildebrandt

This library is free software; you can redistribute it and/or
modify it under the terms of the Lisp Lesser GNU Public License
 (http://opensource.franz.com/preamble.html), known as the LLGPL.

This library is distributed  WITHOUT ANY WARRANTY; without even 
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  

See the Lisp Lesser GNU Public License for more details.

|#


(in-package :c-ode)


;;; 
;;; init & cleanup
;;; 

(export! ode-init ode-cleanup ode-step)

(defun ode-init ()
  (when *objects* (ode-cleanup))
  (ode:init-ode)  
  (setf *objects* (make-hash-table)
	*space* (make-instance 'hash-space))
  (make-instance 'world))


(defun ode-cleanup ()
  ;; destroy objects in the right order
  (when *objects*
   (loop for obj being the hash-values of *objects*
      with sorted = (make-hash-table)
      do (push obj (gethash (major-class-name obj) sorted))
      finally (dolist (class +ode-classes+)
		(mapcar #'ode-destroy (gethash class sorted)))))
  (setf *objects* nil
        *space* nil)
  (ode:close-ode))

;;;
;;; stepping
;;;


(defun ode-step (&key (step-size 0.01) (diag t) (fast-step nil) (max-iterations 20))
  "steps the world by step-size seconds"
    
  (with-collision (*space*)
    (if fast-step
	(step-fast1 *world* step-size max-iterations)
	(step *world* step-size)))

  (dohash (obj *objects*) (update obj))
 
  (when diag
    (format t "~&-----------~%  OBJECTS~%-----------~%")
    (dohash (obj *objects*) (print obj))
    (format t "~&-----------~%END OBJECTS~%----- ------~%")))



